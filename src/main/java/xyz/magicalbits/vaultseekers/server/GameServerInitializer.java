/**
 * Vault Seekers Server
 * Copyright (C) 2023  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.server;


import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.compression.ZlibCodecFactory;
import io.netty.handler.codec.compression.ZlibWrapper;
import io.netty.handler.ssl.SslContext;
import xyz.magicalbits.vaultseekers.common.MessageDecoder;
import xyz.magicalbits.vaultseekers.common.MessageEncoder;
import xyz.magicalbits.vaultseekers.server.core.GameLogic;
import xyz.magicalbits.vaultseekers.server.core.MessageHandler;

/** Creates a newly configured {@link ChannelPipeline} for a server-side channel. */
public class GameServerInitializer extends ChannelInitializer<SocketChannel> {

  private final SslContext sslCtx;
  private final GameLogic gameLogic;
  private final MessageHandler messageHandler;

  public GameServerInitializer(
      SslContext sslCtx, GameLogic gameLogic, MessageHandler messageHandler) {
    this.sslCtx = sslCtx;
    this.gameLogic = gameLogic;
    this.messageHandler = messageHandler;
  }

  @Override
  public void initChannel(SocketChannel ch) {
    ChannelPipeline pipeline = ch.pipeline();

    if (sslCtx != null) {
      pipeline.addLast(sslCtx.newHandler(ch.alloc()));
    }

    // Enable stream compression.
    pipeline.addLast(ZlibCodecFactory.newZlibEncoder(ZlibWrapper.GZIP));
    pipeline.addLast(ZlibCodecFactory.newZlibDecoder(ZlibWrapper.GZIP));

    // Add the number codec first,
    pipeline.addLast(new MessageDecoder());
    pipeline.addLast(new MessageEncoder());

    // and then business logic.
    // Please note we create a handler for every new channel
    // because it has stateful properties.
    pipeline.addLast(createServerHandler());
  }

  private GameServerHandler createServerHandler() {
    return new GameServerHandler(gameLogic, messageHandler);
  }
}
