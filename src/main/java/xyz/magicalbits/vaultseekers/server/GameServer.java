/**
 * Vault Seekers Server
 * Copyright (C) 2023  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.server;


import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import xyz.magicalbits.vaultseekers.common.util.ServerUtil;
import xyz.magicalbits.vaultseekers.server.core.GameLogic;
import xyz.magicalbits.vaultseekers.server.core.GameState;
import xyz.magicalbits.vaultseekers.server.core.LocationValidator;
import xyz.magicalbits.vaultseekers.server.core.MessageAuthorizer;
import xyz.magicalbits.vaultseekers.server.core.MessageHandler;
import xyz.magicalbits.vaultseekers.server.network.NetworkManager;

/**
 * Binds to a local port and creates a {@link NioEventLoopGroup} for {@code NetworkClient} instances
 * to connect to.
 */
public final class GameServer {

  static final int PORT = Integer.parseInt(System.getProperty("port", "8322"));

  public static void main(String[] args) throws Exception {
    // Configure SSL.
    final SslContext sslCtx = ServerUtil.buildSslContext();

    GameServerInitializer serverInitializer = createGameServerInitializer(sslCtx);

    EventLoopGroup bossGroup = new NioEventLoopGroup(1);
    EventLoopGroup workerGroup = new NioEventLoopGroup();
    try {
      ServerBootstrap b = new ServerBootstrap();
      b.group(bossGroup, workerGroup)
          .channel(NioServerSocketChannel.class)
          .handler(new LoggingHandler(LogLevel.INFO))
          .childHandler(serverInitializer);

      b.bind(PORT).sync().channel().closeFuture().sync();
    } finally {
      bossGroup.shutdownGracefully();
      workerGroup.shutdownGracefully();
    }
  }

  private static GameServerInitializer createGameServerInitializer(SslContext sslCtx) {
    NetworkManager networkManager = new NetworkManager();
    MessageAuthorizer messageAuthorizer = new MessageAuthorizer(networkManager);
    GameLogic gameLogic = new GameLogic(new GameState(), networkManager, messageAuthorizer);
    MessageHandler messageHandler =
        new MessageHandler(
            messageAuthorizer, new LocationValidator(gameLogic), gameLogic, networkManager);
    return new GameServerInitializer(sslCtx, gameLogic, messageHandler);
  }
}
