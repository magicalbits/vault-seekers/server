/**
 * Vault Seekers Server
 * Copyright (C) 2023  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.server.core;


import java.util.List;
import java.util.Objects;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.Location;
import xyz.magicalbits.vaultseekers.common.card.ColorfulCard;
import xyz.magicalbits.vaultseekers.common.card.DialogCard;
import xyz.magicalbits.vaultseekers.common.card.GemCard;
import xyz.magicalbits.vaultseekers.common.card.HiddenLootVaultCard;
import xyz.magicalbits.vaultseekers.common.card.PointCard;
import xyz.magicalbits.vaultseekers.common.card.SecretCard;
import xyz.magicalbits.vaultseekers.common.card.VaultCard;
import xyz.magicalbits.vaultseekers.common.proto.Message;
import xyz.magicalbits.vaultseekers.server.Player;
import xyz.magicalbits.vaultseekers.server.network.NetworkManager;
import xyz.magicalbits.vaultseekers.server.utils.Utils;

@Log4j2
public class ConflictResolver implements Runnable {
  static final int RESOLVE_CONFLICT_DELAY_MILLIS = 1000;
  private final GameLogic gameLogic;
  private final NetworkManager networkManager;
  private final MessageAuthorizer messageAuthorizer;

  public ConflictResolver(
      GameLogic gameLogic, NetworkManager networkManager, MessageAuthorizer messageAuthorizer) {
    this.gameLogic = gameLogic;
    this.networkManager = networkManager;
    this.messageAuthorizer = messageAuthorizer;
  }

  @Override
  public void run() {
    try {
      resolveConflict();
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }

  private void resolveConflict() throws InterruptedException {
    log.info("Resolving conflict.");
    for (Message.Location.Builder loc : gameLogic.getLocations()) {
      // Sleep for a while to let the player notice location changes.
      Thread.sleep(RESOLVE_CONFLICT_DELAY_MILLIS);

      List<Message.Card> attackCards = loc.getAttackCardsList();
      List<Message.Card> defenseCards = loc.getDefenseCardsList();

      if (!defenseCards.isEmpty()) {
        // No one gets anything, regardless of the number of attackers. Vault card stays.
        loc.clearAttackCards().clearDefenseCards();
      } else if (attackCards.isEmpty()) {
        // Vault card will be replaced.
        loc.clearVaultCardPath().setShouldDeleteVaultCard(true);
        gameLogic.updateVaultCardSlot(Location.Name.valueOf(loc.getName()), null);
      } else if (attackCards.size() == 1) {
        // The attacker gets the loot and the vault card will be replaced.
        Player attacker =
            Objects.requireNonNull(gameLogic.getPlayer(attackCards.get(0).getColor()));
        VaultCard vaultCard = gameLogic.getVaultCardFromSlot(Location.Name.valueOf(loc.getName()));
        giveLoot(attacker, vaultCard);
        if (vaultCard instanceof DialogCard) {
          log.debug(
              "Suspending conflict resolver. Waiting for player {} to close the dialog.",
              attacker.getColor());
          synchronized (this) {
            wait();
          }
          log.debug("Conflict resolver resumed.");
        }
        loc.clearVaultCardPath().setShouldDeleteVaultCard(true).clearAttackCards();
        gameLogic.updateVaultCardSlot(Location.Name.valueOf(loc.getName()), null);
      } else {
        // Each attacker gets a hidden loot card. Vault card stays.
        for (Player p : gameLogic.getPlayers()) {
          p.giveHiddenLootCards(1);
          networkManager.sendMessage(
              Utils.createMessageWithTotalHiddenLootCardCount(p), gameLogic.getPlayers());
        }

        loc.clearAttackCards();
      }

      gameLogic.sendLocationUpdate();
      loc.clearShouldDeleteVaultCard();
    }

    // Sleep to make the transition to the next round smooth.
    Thread.sleep(RESOLVE_CONFLICT_DELAY_MILLIS);

    gameLogic.nextRound();
  }

  private void giveLoot(Player player, VaultCard vaultCard) {
    if (vaultCard instanceof ColorfulCard) {
      player.addOneOfEachGems();
      networkManager.sendMessage(
          Utils.createMessageWithUpdatedGems(player), gameLogic.getPlayers());
    } else if (vaultCard instanceof DialogCard card) {
      Message.Dialog.Type dialogType = Message.Dialog.Type.UNRECOGNIZED;
      switch (card.getType()) {
        case THREE_GEMS -> dialogType = Message.Dialog.Type.THREE_GEMS;
        case TRADE -> dialogType = Message.Dialog.Type.TRADE;
      }

      if (dialogType.equals(Message.Dialog.Type.UNRECOGNIZED)) {
        throw new RuntimeException("Invalid dialog type.");
      }

      // TODO when server sends SHOW_DIALOG, show a message about it to other players, preferably in
      //  top center of the screen
      messageAuthorizer.authorizeMessageType(player, Message.Type.SHOW_DIALOG);
      messageAuthorizer.authorizeDialogType(player, dialogType);
      networkManager.sendMessage(
          Message.newBuilder()
              .setType(Message.Type.SHOW_DIALOG)
              .setDialog(Message.Dialog.newBuilder().setType(dialogType).build())
              .build(),
          player);
    } else if (vaultCard instanceof GemCard card) {
      player.addGems(card.getColor(), card.getPoints());
      networkManager.sendMessage(
          Utils.createMessageWithUpdatedGems(player), gameLogic.getPlayers());
    } else if (vaultCard instanceof HiddenLootVaultCard) {
      player.giveHiddenLootCards(3);
      networkManager.sendMessage(
          Utils.createMessageWithTotalHiddenLootCardCount(player), gameLogic.getPlayers());
    } else if (vaultCard instanceof PointCard card) {
      player.givePoints(card.getPoints());
      networkManager.sendMessage(
          Utils.createMessageWithUpdatedPoints(player), gameLogic.getPlayers());
    } else if (vaultCard instanceof SecretCard card) {
      for (Player other : gameLogic.getPlayers()) {
        if (!player.getColor().equals(other.getColor())) {
          log.info("Taking half of {} gems from player {}.", card.getColor(), other.getColor());
          final int amount = other.getGemAmount(card.getColor());
          // Take half of the player's gems of given color. In case of an odd number of gems, take
          // the bigger half.
          other.subtractGems(card.getColor(), (int) Math.ceil(amount / 2.));
          networkManager.sendMessage(
              Utils.createMessageWithUpdatedGems(other), gameLogic.getPlayers());
        }
      }
    }
  }

  public synchronized void resume() {
    notify();
  }
}
