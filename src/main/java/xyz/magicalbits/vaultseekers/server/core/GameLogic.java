/**
 * Vault Seekers Server
 * Copyright (C) 2024  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.server.core;


import io.netty.channel.ChannelHandlerContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.TreeMap;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.Location;
import xyz.magicalbits.vaultseekers.common.card.ColorfulCard;
import xyz.magicalbits.vaultseekers.common.card.DialogCard;
import xyz.magicalbits.vaultseekers.common.card.GemCard;
import xyz.magicalbits.vaultseekers.common.card.HiddenLootVaultCard;
import xyz.magicalbits.vaultseekers.common.card.PointCard;
import xyz.magicalbits.vaultseekers.common.card.SecretCard;
import xyz.magicalbits.vaultseekers.common.card.VaultCard;
import xyz.magicalbits.vaultseekers.common.proto.Gem;
import xyz.magicalbits.vaultseekers.common.proto.Message;
import xyz.magicalbits.vaultseekers.server.GameServerHandler;
import xyz.magicalbits.vaultseekers.server.Player;
import xyz.magicalbits.vaultseekers.server.network.NetworkManager;
import xyz.magicalbits.vaultseekers.server.utils.Utils;

@Log4j2
public class GameLogic {
  private final GameState gameState;
  private final NetworkManager networkManager;
  private final MessageAuthorizer messageAuthorizer;

  @Getter
  private final List<Player> players =
      Collections.synchronizedList(new ArrayList<>(GameServerHandler.PLAYER_LIMIT));

  private static final Random random = new Random();

  public GameLogic(
      GameState gameState, NetworkManager networkManager, MessageAuthorizer messageAuthorizer) {
    this.gameState = gameState;
    this.networkManager = networkManager;
    this.messageAuthorizer = messageAuthorizer;
  }

  private List<Message.PlayerStats> getAllPlayerStats() {
    List<Message.PlayerStats> allStats = new ArrayList<>(getPlayers().size());
    for (Player p : getPlayers()) {
      Message.PlayerStats stats =
          Message.PlayerStats.newBuilder()
              .setColor(p.getColor())
              .setPoints(p.getPoints())
              .addAllGems(Utils.convertGemMapToList(p.getGems()))
              .addAllHiddenLootCards(Utils.convertHiddenLootCardMapToList(p.getHiddenLootCards()))
              .addAllScores(Utils.convertScoreMapToList(p.getScores()))
              .setWon(p.isWinner())
              .build();
      allStats.add(stats);
    }
    return allStats;
  }

  public Player getPlayer(ChannelHandlerContext ctx) {
    for (Player p : getPlayers()) {
      if (p.getChannel().equals(ctx.channel())) {
        return p;
      }
    }
    throw new IllegalStateException(
        "Couldn't find player with IP address %s".formatted(ctx.channel().remoteAddress()));
  }

  public Player getPlayer(Message.PlayerColor playerColor) {
    for (Player p : getPlayers()) {
      if (p.getColor().equals(playerColor)) {
        return p;
      }
    }
    throw new IllegalStateException("Couldn't find player with color %s".formatted(playerColor));
  }

  public void updateLocations(List<Message.Location> newLocations) {
    newLocations.forEach(
        newLoc -> {
          Optional<Message.Location.Builder> foundLoc =
              gameState.getLocations().stream()
                  .filter(loc -> loc.getName().equalsIgnoreCase(newLoc.getName()))
                  .findFirst();
          if (foundLoc.isPresent()) {
            Message.Location.Builder loc = foundLoc.get();
            if (!newLoc.getAttackCardsList().isEmpty()) {
              loc.addAllAttackCards(new ArrayList<>(newLoc.getAttackCardsList()));
            }
            if (!newLoc.getDefenseCardsList().isEmpty()) {
              loc.addAllDefenseCards(new ArrayList<>(newLoc.getDefenseCardsList()));
            }
          } else {
            // This should never happen since all locations were validated.
            throw new RuntimeException("Location '%s' not found.".formatted(newLoc.getName()));
          }
        });
  }

  List<Message.Location> getBuiltLocations() {
    return gameState.getLocations().stream().map(Message.Location.Builder::build).toList();
  }

  public void nextRound() {
    // Place vault cards in case any were removed or, if the game isn't running anymore, proceed to
    // the game end screen showing the winner(s).
    placeVaultCards();
    if (gameState.isGameRunning()) {
      assignExtraBoss();
      networkManager.sendMessage(
          Message.newBuilder().setType(Message.Type.ANNOUNCE_NEW_ROUND).build(), players);
      getPlayers()
          .forEach(p -> messageAuthorizer.authorizeMessageType(p, Message.Type.UPDATE_LOCATIONS));
    } else {
      // There are no vault cards left to place. Announce the game is over and reveal winner(s).
      log.info("Announcing game end.");
      determineWinners();
      networkManager.sendMessage(
          Message.newBuilder()
              .setType(Message.Type.ANNOUNCE_GAME_END)
              .addAllPlayerStats(getAllPlayerStats())
              .build(),
          players);
    }
  }

  /** Calculates each player's score depending on the gem amounts and marks winning players. */
  private void determineWinners() {
    Map<Player, Integer> scoreMap = new HashMap<>(getPlayers().size());

    for (Gem.Color gemColor : Gem.Color.values()) {
      if (gemColor.equals(Gem.Color.UNRECOGNIZED)) {
        continue;
      }

      // Sort players in descending order by gem count (including hidden loot cards) of
      // given gem color.
      List<Player> sortedPlayers = new ArrayList<>(getPlayers());
      sortedPlayers.sort(
          Comparator.comparingInt((Player p) -> p.getCompleteGemAmount(gemColor)).reversed());

      // Group players by gem amount (which includes gems from HiddenLootCards).
      Map<Integer, List<Player>> groupedPlayers = new TreeMap<>(Comparator.reverseOrder());
      for (Player player : sortedPlayers) {
        final int completeGemAmount = player.getCompleteGemAmount(gemColor);
        List<Player> value = groupedPlayers.getOrDefault(completeGemAmount, new ArrayList<>());
        value.add(player);
        groupedPlayers.put(completeGemAmount, value);
      }

      // Sum score each player would get depending on their position, and divide the sum
      // between all players equally (sum 5 divided by 2 players equals to 2 gems to each
      // player -- leftover gems are discarded).
      List<Integer> scoreRewards =
          new ArrayList<>(Utils.getScoreRewards(gemColor, getPlayers().size()));
      for (Map.Entry<Integer, List<Player>> entry : groupedPlayers.entrySet()) {
        List<Player> playerGroup = entry.getValue();
        List<Integer> scoreSubList = scoreRewards.subList(0, playerGroup.size());
        final int scoreSum = scoreSubList.stream().reduce(Integer::sum).orElseThrow();
        final int splitScore = Math.floorDiv(scoreSum, playerGroup.size());
        for (Player p : playerGroup) {
          scoreMap.put(p, scoreMap.getOrDefault(p, 0) + splitScore);
          p.addScore(gemColor, splitScore);
          log.info(
              "Incremented {} player's score for {} gems by {}.",
              p.getColor(),
              gemColor,
              splitScore);
        }

        scoreSubList.clear();
      }
    }

    // Mark players with the highest score as winners.
    Optional<Integer> highestScore = scoreMap.values().stream().max(Comparator.naturalOrder());
    log.debug("Highest score: {}", highestScore);
    if (getPlayers().size() != 2 && highestScore.isPresent()) {
      scoreMap.entrySet().stream()
          .filter(entry -> entry.getValue().equals(highestScore.get()))
          .forEach(entry -> entry.getKey().setWinner(true));
    } else if (highestScore.isPresent()) {
      List<Integer> values = List.copyOf(scoreMap.values());
      if (Math.abs(values.get(0) - values.get(1)) < 2) {
        // Tie.
        log.info("Both players won (tie).");
        getPlayers().forEach(p -> p.setWinner(true));
      } else {
        // One winner.
        Player player =
            scoreMap.entrySet().stream().max(Map.Entry.comparingByValue()).orElseThrow().getKey();
        player.setWinner(true);
        log.info("Player {} won.", player.getColor());
      }
    }
  }

  private void initLocations() {
    for (int i = 0; i < gameState.getLocationCount(); i++) {
      gameState
          .getLocations()
          .add(Message.Location.newBuilder().setName(Location.Name.values()[i].toString()));
    }
  }

  private void initVaultCards() {
    String vaultCardPath = "cards/vault/vault";
    for (int i = 0; i < Gem.Color.values().length - 1; i++) {
      for (int j = 0; j < 3; j++) {
        String vault;
        // Vault 1 cards' points are always in the range of <2; 4>.
        int points = j + 2;
        if (points == 2) {
          vault = "1";
        } else {
          vault = "12";
        }
        Gem.Color color = Gem.Color.values()[i];
        gameState
            .getVaultOneCards()
            .add(
                new GemCard(
                    vaultCardPath
                        + vault
                        + "_"
                        + color.toString().toLowerCase()
                        + "_"
                        + points
                        + ".png",
                    color,
                    points));
      }
    }

    for (int i = 0; i < Gem.Color.values().length - 1; i++) {
      for (int j = 0; j < 3; j++) {
        String vault;
        int points = j + 3;
        if (points == 5) {
          vault = "2";
        } else {
          vault = "12";
        }
        Gem.Color color = Gem.Color.values()[i];
        gameState
            .getVaultTwoCards()
            .add(
                new GemCard(
                    vaultCardPath
                        + vault
                        + "_"
                        + color.toString().toLowerCase()
                        + "_"
                        + points
                        + ".png",
                    color,
                    points));
      }
    }

    gameState.getVaultOneCards().add(new ColorfulCard(vaultCardPath + "12_colorful.png"));
    gameState.getVaultOneCards().add(new ColorfulCard(vaultCardPath + "12_colorful.png"));
    gameState
        .getVaultOneCards()
        .add(new DialogCard(vaultCardPath + "1_any_3.png", DialogCard.Type.THREE_GEMS));
    gameState
        .getVaultOneCards()
        .add(new DialogCard(vaultCardPath + "1_any_3.png", DialogCard.Type.THREE_GEMS));
    gameState
        .getVaultOneCards()
        .add(new HiddenLootVaultCard(vaultCardPath + "1_hidden_loot_3.png"));
    gameState
        .getVaultOneCards()
        .add(new HiddenLootVaultCard(vaultCardPath + "1_hidden_loot_3.png"));
    gameState.getVaultOneCards().add(new PointCard(vaultCardPath + "12_point_1.png", 1));
    gameState.getVaultOneCards().add(new PointCard(vaultCardPath + "12_point_1.png", 1));

    gameState.getVaultTwoCards().add(new ColorfulCard(vaultCardPath + "12_colorful.png"));
    gameState.getVaultTwoCards().add(new ColorfulCard(vaultCardPath + "12_colorful.png"));
    gameState.getVaultTwoCards().add(new PointCard(vaultCardPath + "12_point_1.png", 1));
    gameState.getVaultTwoCards().add(new PointCard(vaultCardPath + "2_point_2.png", 2));
    gameState
        .getVaultTwoCards()
        .add(new DialogCard(vaultCardPath + "2_trade_3.png", DialogCard.Type.TRADE));
    gameState
        .getVaultTwoCards()
        .add(new DialogCard(vaultCardPath + "2_trade_3.png", DialogCard.Type.TRADE));
    gameState
        .getVaultTwoCards()
        .add(new SecretCard(vaultCardPath + "2_blue_secret_docs.png", Gem.Color.BLUE));
    gameState
        .getVaultTwoCards()
        .add(new SecretCard(vaultCardPath + "2_green_secret_docs.png", Gem.Color.GREEN));
    gameState
        .getVaultTwoCards()
        .add(new SecretCard(vaultCardPath + "2_red_secret_docs.png", Gem.Color.RED));
    gameState
        .getVaultTwoCards()
        .add(new SecretCard(vaultCardPath + "2_yellow_secret_docs.png", Gem.Color.YELLOW));

    // debug
    // gameState.getVaultOneCards().subList(0, 20).clear();
    // gameState.getVaultTwoCards().subList(0, 16).clear();
  }

  void placeVaultCards() {
    boolean anyVaultCardPlaced = false;
    for (Message.Location.Builder loc : gameState.getLocations()) {
      if (!loc.hasVaultCardPath() && !gameState.getVaultOneCards().isEmpty()) {
        // Assign a VaultCard from vault 1.

        VaultCard card =
            gameState.getVaultOneCards().remove(gameState.getVaultOneCards().size() - 1);
        loc.setVaultCardPath(card.getFilePath());
        gameState.getVaultCardSlots().put(Location.Name.valueOf(loc.getName()), card);
        log.debug("Set vault one card at location {} to '{}'.", loc.getName(), card.getFilePath());
        anyVaultCardPlaced = true;
      } else if (!loc.hasVaultCardPath() && !gameState.getVaultTwoCards().isEmpty()) {
        // Assign a VaultCard from vault 2.

        VaultCard card =
            gameState.getVaultTwoCards().remove(gameState.getVaultTwoCards().size() - 1);
        loc.setVaultCardPath(card.getFilePath());
        gameState.getVaultCardSlots().put(Location.Name.valueOf(loc.getName()), card);
        log.debug(
            "Set a vault two card at location {} to '{}'.", loc.getName(), card.getFilePath());
        anyVaultCardPlaced = true;
      } else if (!loc.hasVaultCardPath()) {
        // There are no more vault cards to be assigned. The game is over.
        gameState.setGameRunning(false);
        gameState.setGameFinished(true);
        break;
      }

      // If none of the conditions above are met, skip this location because it still has a
      // VaultCard.
    }

    if (anyVaultCardPlaced) {
      sendLocationUpdate();
    }
  }

  private void initPlayerColors() {
    gameState.getAvailablePlayerColors().addAll(Utils.getPlayerColors());
  }

  private void assignPlayerColors() {
    for (Player p : getPlayers()) {
      Message.PlayerColor color = gameState.getAvailablePlayerColors().remove(0);
      p.setColor(color);
      networkManager.sendMessage(
          Message.newBuilder()
              .setType(Message.Type.SET_PLAYER_COLOR)
              .addPlayerStats(Message.PlayerStats.newBuilder().setColor(color).build())
              .build(),
          p);
    }
  }

  void assignExtraBoss() {
    final Message.Builder noBossMsg =
        Message.newBuilder()
            .setType(Message.Type.UPDATE_EXTRA_BOSS)
            .setPlacedAttackCardsLimit(1)
            .setPlacedDefenseCardsLimit(2);

    if (getPlayers().size() != 2) {
      networkManager.sendMessage(noBossMsg.setPlacedDefenseCardsLimit(1).build(), players);
      return;
    }

    final Message bossMsg =
        Message.newBuilder()
            .setType(Message.Type.UPDATE_EXTRA_BOSS)
            .setExtraBoss(
                Message.Card.newBuilder()
                    .setPath(
                        "cards/boss/boss_"
                            + gameState.getAvailablePlayerColors().get(0).toString().toLowerCase()
                            + ".png")
                    .build())
            .setPlacedAttackCardsLimit(2)
            .setPlacedDefenseCardsLimit(1)
            .build();
    boolean bossExists = false;
    for (Player p : getPlayers()) {
      if (p.isExtraBoss()) {
        bossExists = true;
        break;
      }
    }

    if (!bossExists) {
      Player p = getPlayers().get(0);
      p.setExtraBoss(true);
      log.info("Assigning extra boss to player {}.", p.getColor());
      networkManager.sendMessage(bossMsg, p);
      networkManager.sendMessage(noBossMsg.build(), getPlayers().get(1));
      return;
    }

    for (Player p : getPlayers()) {
      boolean hasExtraBoss = !p.isExtraBoss();
      p.setExtraBoss(hasExtraBoss);
      if (hasExtraBoss) {
        log.info("Assigning extra boss to player {}.", p.getColor());
        networkManager.sendMessage(bossMsg, p);
      } else {
        log.info("Removing extra boss from player {}.", p.getColor());
        networkManager.sendMessage(noBossMsg.build(), p);
      }
    }
  }

  private void sendRandomGems() {
    // Give three random gems to each player.
    Map<Gem.Color, Integer> gems = new EnumMap<>(Gem.Color.class);
    for (Player player : getPlayers()) {
      gems.clear();
      for (int i = 0; i < 3; i++) {
        int randomNum = random.nextInt(Gem.Color.values().length - 1);
        Gem.Color color = Gem.Color.values()[randomNum];
        gems.put(color, gems.getOrDefault(color, 0) + 1);
      }
      player.addGems(gems);
      networkManager.sendMessage(Utils.createMessageWithUpdatedGems(player), players);
    }
  }

  public void startGame() {
    gameState.clear();
    gameState.setGameRunning(true);
    gameState.setLocationCount(calculateLocationCount());
    initLocations();
    initVaultCards();
    gameState.shuffleVaultCards();
    placeVaultCards();
    initPlayerColors();
    assignPlayerColors();
    assignExtraBoss();
    sendRandomGems();
    getPlayers()
        .forEach(p -> messageAuthorizer.authorizeMessageType(p, Message.Type.UPDATE_LOCATIONS));
  }

  private int calculateLocationCount() {
    return switch (getPlayers().size()) {
      case 2, 3 -> 6;
      case 4 -> 7;
      case 5 -> 8;
      default -> throw new IllegalArgumentException(
          "Invalid player limit. Should be within [2, 5].");
    };
  }

  void disconnectAllOtherPlayers(ChannelHandlerContext ctx) {
    getPlayers().stream()
        .filter(p -> !p.getChannel().equals(ctx.channel()))
        .forEach(p -> networkManager.disconnect(p, "A player left the game. The game is over."));
    getPlayers().clear();
  }

  void updatePlayerCount() {
    if (!getPlayers().isEmpty()) {
      networkManager.sendMessage(
          Message.newBuilder()
              .setType(Message.Type.UPDATE_PLAYER_COUNT)
              .addAllPlayerStats(
                  getPlayers().stream().map(p -> Message.PlayerStats.newBuilder().build()).toList())
              .build(),
          players);
    }
  }

  public void addPlayer(Player player) {
    if (getPlayers().size() >= GameServerHandler.PLAYER_LIMIT
        || gameState.isGameRunning()
        || gameState.isGameFinished()) {
      networkManager.disconnect(player, "The server is full. Disconnected.");
      return;
    }

    getPlayers().add(player);
    if (getPlayers().size() == 1) {
      player.setHost();
      messageAuthorizer.authorizeMessageType(player, Message.Type.START_GAME);
      networkManager.sendMessage(
          Message.newBuilder().setType(Message.Type.START_GAME).build(), player);
    }
    log.debug("Player added. Total players: {}", getPlayers().size());
    updatePlayerCount();
    // Once the server becomes full, start the game.
    // TODO this might not be expected behavior
    if (getPlayers().size() == GameServerHandler.PLAYER_LIMIT) {
      startGame();
    }
  }

  public void removePlayer(ChannelHandlerContext ctx) {
    if (gameState.isGameRunning()) {
      gameState.setGameRunning(false);
      disconnectAllOtherPlayers(ctx);
    } else {
      for (Player p : getPlayers()) {
        if (p.getChannel().equals(ctx.channel())) {
          if (p.getColor() != null) {
            gameState.getAvailablePlayerColors().add(p.getColor());
            log.debug("Color {} was added back to the list of available colors.", p.getColor());
          }
          getPlayers().remove(p);
          log.debug("Player removed. Total players: {}", getPlayers().size());
          if (!gameState.isGameRunning() && !gameState.isGameFinished()) {
            if (p.isHost() && !getPlayers().isEmpty()) {
              Player player = getPlayers().get(0);
              player.setHost();
              networkManager.sendMessage(
                  Message.newBuilder().setType(Message.Type.START_GAME).build(), player);
            }
            updatePlayerCount();
          } else if (!gameState.isGameRunning() && getPlayers().isEmpty()) {
            // When the game finishes and the last player leaves, make the server available again.
            gameState.setGameFinished(false);
          }
          break;
        }
      }
    }
  }

  public void sendLocationUpdate() {
    log.info("Sending location update.");
    networkManager.sendMessage(
        Message.newBuilder()
            .setType(Message.Type.UPDATE_LOCATIONS)
            .addAllLocations(getBuiltLocations())
            .build(),
        players);
  }

  public void updateVaultCardSlot(Location.Name locName, VaultCard card) {
    gameState.getVaultCardSlots().put(locName, card);
  }

  public VaultCard getVaultCardFromSlot(Location.Name locName) {
    return gameState.getVaultCardSlots().get(locName);
  }

  public List<Message.Location.Builder> getLocations() {
    return gameState.getLocations();
  }

  public int getLocationCount() {
    return gameState.getLocationCount();
  }
}
