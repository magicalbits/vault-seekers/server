/**
 * Vault Seekers Server
 * Copyright (C) 2024  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.server.core;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.proto.Message;
import xyz.magicalbits.vaultseekers.server.GameServerHandler;
import xyz.magicalbits.vaultseekers.server.Player;
import xyz.magicalbits.vaultseekers.server.network.NetworkManager;

@Log4j2
public class MessageAuthorizer {
  private final NetworkManager networkManager;
  private final Map<Player, List<Message.Type>> authorizedMessageTypes =
      new HashMap<>(GameServerHandler.PLAYER_LIMIT);
  private final Map<Player, List<Message.Dialog.Type>> authorizedDialogTypes =
      new HashMap<>(GameServerHandler.PLAYER_LIMIT);

  public MessageAuthorizer(NetworkManager networkManager) {
    this.networkManager = networkManager;
  }

  public boolean isMessageTypeAuthorized(Player player, Message msg) {
    if (!isMessageTypeAuthorized(player, msg.getType())) {
      networkManager.disconnect(
          player, "Received an unauthorized message type %s.".formatted(msg.getType()));
      return false;
    } else if (msg.getType().equals(Message.Type.SHOW_DIALOG)) {
      if (!msg.hasDialog()) {
        networkManager.disconnect(player, "Received a dialog message without dialog metadata.");
        return false;
      }
      if (!msg.getDialog().hasType()) {
        networkManager.disconnect(
            player, "Received a dialog message without the dialog type field.");
        return false;
      }
      if (!isDialogTypeAuthorized(player, msg.getDialog().getType())) {
        networkManager.disconnect(
            player,
            "Received an unauthorized dialog type %s.".formatted(msg.getDialog().getType()));
        return false;
      }
    }
    return true;
  }

  public boolean isMessageTypeAuthorized(Player player, Message.Type type) {
    return authorizedMessageTypes.containsKey(player)
        && authorizedMessageTypes.get(player).contains(type);
  }

  public boolean isDialogTypeAuthorized(Player player, Message.Dialog.Type type) {
    return authorizedDialogTypes.containsKey(player)
        && authorizedDialogTypes.get(player).contains(type);
  }

  public void authorizeMessageType(Player player, Message.Type type) {
    String id =
        player.getColor() == null
            ? "client %s".formatted(player.getChannel().remoteAddress())
            : "player %s".formatted(player.getColor());
    log.info("Authorizing {} to send a message of type {}.", id, type);
    authorizeType(
        type, authorizedMessageTypes.computeIfAbsent(player, p -> new ArrayList<>()), "Message");
  }

  public void authorizeDialogType(Player player, Message.Dialog.Type type) {
    log.info(
        "Authorizing player {} to send a dialog response of type {}.", player.getColor(), type);
    authorizeType(
        type, authorizedDialogTypes.computeIfAbsent(player, p -> new ArrayList<>()), "Dialog");
  }

  private static <T> void authorizeType(T type, List<T> types, String description) {
    if (types.contains(type)) {
      log.warn("{} type already authorized.", description);
    } else {
      types.add(type);
    }
  }

  public void revokeAuthorizedMessageType(Player player, Message.Type type) {
    log.info(
        "Revoking authorization of player {} to send a message of type {}.",
        player.getColor(),
        type);
    revokeAuthorizedType(
        type, authorizedMessageTypes.computeIfAbsent(player, p -> new ArrayList<>()));
  }

  public void revokeAuthorizedDialogType(Player player, Message.Dialog.Type type) {
    log.info(
        "Revoking authorization of player {} to send a dialog response of type {}.",
        player.getColor(),
        type);
    revokeAuthorizedType(
        type, authorizedDialogTypes.computeIfAbsent(player, p -> new ArrayList<>()));
  }

  private static <T> void revokeAuthorizedType(T type, List<T> types) {
    if (!types.remove(type)) {
      String msg =
          "Cannot revoke a previously unauthorized type " + type.getClass().getSimpleName();
      log.error(msg);
      throw new IllegalStateException(msg);
    }
  }
}
