/**
 * Vault Seekers Server
 * Copyright (C) 2024  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.server.core;


import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.proto.Gem;
import xyz.magicalbits.vaultseekers.common.proto.Message;
import xyz.magicalbits.vaultseekers.server.GameServerHandler;
import xyz.magicalbits.vaultseekers.server.InvalidMessageValueException;
import xyz.magicalbits.vaultseekers.server.Player;
import xyz.magicalbits.vaultseekers.server.network.NetworkManager;
import xyz.magicalbits.vaultseekers.server.utils.Utils;

@Log4j2
@RequiredArgsConstructor
public class MessageHandler {
  private final MessageAuthorizer messageAuthorizer;
  private final LocationValidator locationValidator;
  private final GameLogic gameLogic;
  private final NetworkManager networkManager;

  @Setter(value = AccessLevel.PRIVATE)
  private ConflictResolver conflictResolver;

  public void process(Message msg, Player player) {
    if (!messageAuthorizer.isMessageTypeAuthorized(player, msg)) {
      return;
    }

    log.info("Processing incoming message of type {}.", msg.getType());
    messageAuthorizer.revokeAuthorizedMessageType(player, msg.getType());
    switch (msg.getType()) {
      case UPDATE_LOCATIONS -> {
        List<Message.Location> incomingLocations = msg.getLocationsList();
        locationValidator.validateLocations(incomingLocations, player);

        List<Message.Location> outgoingLocations = new ArrayList<>();
        for (Message.Location incomingLocation : incomingLocations) {
          // If the client happens to send a vault card path, don't pass it to other players.
          if (incomingLocation.hasVaultCardPath()) {
            incomingLocation = incomingLocation.toBuilder().clearVaultCardPath().build();
            outgoingLocations.add(incomingLocation);
          } else {
            outgoingLocations.add(
                Message.Location.newBuilder().mergeFrom(incomingLocation).build());
          }
        }

        gameLogic.updateLocations(outgoingLocations);

        player.setTurnEnded(true);

        // If all players ended their turn, send them all a message with the updated locations.
        if (gameLogic.getPlayers().stream().allMatch(Player::isTurnEnded)) {
          gameLogic.sendLocationUpdate();
          gameLogic.getPlayers().forEach(p -> p.setTurnEnded(false));

          // Run ConflictResolver in a new thread. If an attacker loots a dialog vault card, the
          // thread will be suspended until the player closes the dialog.
          setConflictResolver(new ConflictResolver(gameLogic, networkManager, messageAuthorizer));
          new Thread(conflictResolver).start();
        }
      }
      case SHOW_DIALOG -> {
        messageAuthorizer.revokeAuthorizedDialogType(player, msg.getDialog().getType());
        log.debug("Dialog response of type {} received.", msg.getDialog().getType());
        conflictResolver.resume();

        switch (msg.getDialog().getType()) {
          case THREE_GEMS -> {
            // Validate gems.
            Map<Gem.Color, Integer> gems = new EnumMap<>(Gem.Color.class);
            for (Gem gem : msg.getPlayerStats(0).getGemsList()) {
              if (gem.getColor().equals(Gem.Color.UNRECOGNIZED)) {
                throw new IllegalArgumentException("Invalid gem color.");
              } else if (gem.getAmount() < 0 || gem.getAmount() > 3) {
                throw new IllegalArgumentException("Invalid gem amount.");
              }

              gems.put(gem.getColor(), gem.getAmount());
            }

            if (gems.values().stream().reduce(Integer::sum).orElse(0) != 3) {
              throw new IllegalArgumentException(
                  "Sum of received gem amounts does not equal three.");
            }

            player.addGems(gems);
            networkManager.sendMessage(
                Utils.createMessageWithUpdatedGems(player), gameLogic.getPlayers());
          }
          case TRADE -> {
            Map<Player, Map<Gem.Color, Integer>> playerGemMap = new HashMap<>();
            Set<Message.PlayerColor> playerSet = new HashSet<>(gameLogic.getPlayers().size());

            // Input validation
            for (Message.PlayerStats playerStats : msg.getPlayerStatsList()) {
              Message.PlayerColor playerColor = playerStats.getColor();

              if (playerColor.equals(Message.PlayerColor.UNRECOGNIZED)) {
                throw new InvalidMessageValueException("Invalid player color");
              }
              if (gameLogic.getPlayers().stream()
                  .map(Player::getColor)
                  .noneMatch(color -> color == playerColor)) {
                throw new InvalidMessageValueException(
                    "Player '%s' is not present".formatted(playerColor));
              }

              Player currentPlayer = gameLogic.getPlayer(playerColor);

              if (!playerSet.add(playerColor)) {
                throw new InvalidMessageValueException(
                    "Duplicate player '%s' found".formatted(playerColor));
              }

              for (Gem gem : playerStats.getGemsList()) {
                Gem.Color gemColor = gem.getColor();
                int gemAmount = gem.getAmount();

                if (gemColor.equals(Gem.Color.UNRECOGNIZED)) {
                  throw new InvalidMessageValueException("Invalid gem color");
                }
                if (gemAmount < 0 || gemAmount > 3) {
                  throw new InvalidMessageValueException(
                      "Invalid gem amount '%d'".formatted(gemAmount));
                }

                Map<Gem.Color, Integer> gemMap =
                    playerGemMap.getOrDefault(currentPlayer, new EnumMap<>(Gem.Color.class));
                playerGemMap.putIfAbsent(currentPlayer, gemMap);

                if (gemMap.containsKey(gemColor)) {
                  throw new InvalidMessageValueException(
                      "Duplicate gem color '%s' found".formatted(gemColor));
                }

                gemMap.put(gemColor, gemAmount);
              }
            }

            // Fill map with empty maps for players which are not part of the trade to prevent NPEs.
            for (Player p : gameLogic.getPlayers()) {
              playerGemMap.putIfAbsent(p, new EnumMap<>(Gem.Color.class));
            }

            int localPlayerGemSum = Utils.getGemSum(playerGemMap.get(player));
            if (localPlayerGemSum < 0 || localPlayerGemSum > 3) {
              throw new InvalidMessageValueException(
                  "Invalid trader gem sum '%d'".formatted(localPlayerGemSum));
            }

            int otherPlayersGemSum = 0;
            for (Player p : gameLogic.getPlayers()) {
              if (p == player) {
                continue;
              }
              otherPlayersGemSum += Utils.getGemSum(playerGemMap.get(p));
            }

            if (otherPlayersGemSum < 0 || otherPlayersGemSum > 3) {
              throw new InvalidMessageValueException(
                  "Invalid gem sum of non-trader players '%d'".formatted(otherPlayersGemSum));
            }

            if (localPlayerGemSum != otherPlayersGemSum) {
              throw new InvalidMessageValueException(
                  "Trader and non-trader gem sums do not match (%d != %d)"
                      .formatted(localPlayerGemSum, otherPlayersGemSum));
            }

            log.debug("{}", msg.getGemLinksList());

            if (msg.getGemLinksCount() != localPlayerGemSum) {
              throw new InvalidMessageValueException(
                  "Gem links count does not equal to the number of gems selected for trading (%d != %d)"
                      .formatted(msg.getGemLinksCount(), localPlayerGemSum));
            }

            // Validate gem links.
            Map<Gem.Color, Integer> convertedGemLinks = new EnumMap<>(Gem.Color.class);
            for (Message.GemLink gemLink : msg.getGemLinksList()) {
              Message.PlayerColor target = gemLink.getTarget();
              if (player.getColor().equals(target)) {
                throw new InvalidMessageValueException(
                    "The trader cannot give gems to themselves.");
              }
              if (gameLogic.getPlayers().stream()
                  .noneMatch(p -> player.getColor().equals(p.getColor()))) {
                throw new InvalidMessageValueException(
                    "Target player is invalid or is not connected.");
              }

              Gem.Color gemColor = gemLink.getSourceGemColor();
              convertedGemLinks.put(gemColor, convertedGemLinks.getOrDefault(gemColor, 0) + 1);
            }
            Map<Gem.Color, Integer> localPlayerGemMap = playerGemMap.get(player);

            if (!Utils.areMapsEqual(localPlayerGemMap, convertedGemLinks)) {
              throw new InvalidMessageValueException(
                  "Gem links map does not equal local player gem map.");
            }

            for (Message.GemLink gemLink : msg.getGemLinksList()) {
              Player targetPlayer = gameLogic.getPlayer(gemLink.getTarget());
              Gem.Color gemColor = gemLink.getSourceGemColor();
              targetPlayer.addGems(gemColor, 1);
              networkManager.sendMessage(
                  Utils.createMessageWithUpdatedGems(targetPlayer), gameLogic.getPlayers());
              if (player.getGems().getOrDefault(gemColor, 0) == 0) {
                throw new InvalidMessageValueException(
                    "Cannot subtract gem %s from trader due to low balance."
                        .formatted(gemColor.toString()));
              }
              player.subtractGems(gemColor, 1);
              networkManager.sendMessage(
                  Utils.createMessageWithUpdatedGems(player), gameLogic.getPlayers());
            }

            // For each player except the local player, make sure each gem pair that will be taken
            // away from the player doesn't exceed the player's gem amount. Finally, subtract each
            // player's gems according to the trader's choosing.
            playerGemMap.entrySet().stream()
                .filter(entry -> !entry.getKey().equals(player))
                .forEach(
                    entry -> {
                      Player otherPlayer = entry.getKey();
                      entry
                          .getValue()
                          .forEach(
                              (gemColor, gemAmount) -> {
                                int currentAmount = otherPlayer.getGemAmount(gemColor);
                                if (gemAmount > currentAmount) {
                                  throw new InvalidMessageValueException(
                                      "Cannot subtract more gems than the player has");
                                }
                                otherPlayer.subtractGems(gemColor, gemAmount);
                                networkManager.sendMessage(
                                    Utils.createMessageWithUpdatedGems(otherPlayer),
                                    gameLogic.getPlayers());
                                player.addGems(gemColor, gemAmount);
                                networkManager.sendMessage(
                                    Utils.createMessageWithUpdatedGems(player),
                                    gameLogic.getPlayers());
                              });
                    });
          }
          default -> log.error(
              "Received an unexpected dialog message of type {}, skipping.",
              msg.getDialog().getType());
        }
      }
      case START_GAME -> {
        if (gameLogic.getPlayers().size() < GameServerHandler.PLAYER_MIN_LIMIT) {
          throw new IllegalArgumentException(
              "Not enough players (%d < %d) to start the game."
                  .formatted(gameLogic.getPlayers().size(), GameServerHandler.PLAYER_MIN_LIMIT));
        }

        if (!player.isHost()) {
          throw new IllegalArgumentException("Only the host can start the game early.");
        }

        gameLogic.startGame();
      }
      default -> {
        log.error("Received an unexpected message of type {}, skipping.", msg.getType());
      }
    }
  }
}
