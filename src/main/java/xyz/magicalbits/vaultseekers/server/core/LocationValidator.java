/**
 * Vault Seekers Server
 * Copyright (C) 2023  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.server.core;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.Location;
import xyz.magicalbits.vaultseekers.common.proto.Message;
import xyz.magicalbits.vaultseekers.server.Player;

@Log4j2
public class LocationValidator {
  private static final int BOSS_MAX_ATTACK_CARDS = 2;
  private static final int NON_BOSS_MAX_ATTACK_CARDS = 1;
  private static final int BOSS_MAX_DEFENSE_CARDS = 1;
  private static final int NON_BOSS_MAX_DEFENSE_CARDS = 2;
  private final GameLogic gameLogic;

  public LocationValidator(GameLogic gameLogic) {
    this.gameLogic = gameLogic;
  }

  public void validateLocations(List<Message.Location> locations, Player player) {
    List<String> processedLocationNames = new ArrayList<>();
    List<String> attackCardPaths = new ArrayList<>();
    List<String> defenseCardPaths = new ArrayList<>();

    log.info("Validating locations.");
    for (Message.Location location : locations) {
      log.debug("Validating location {}.", location.getName());

      // Validate location name.
      if (Arrays.stream(Location.Name.values())
          .noneMatch(name -> location.getName().equalsIgnoreCase(name.toString()))) {
        throw new RuntimeException("Invalid location name '%s'.".formatted(location.getName()));
      }

      // Check for locations that aren't used in this match.
      Optional<String> unusedLocation =
          List.of(Location.Name.values())
              .subList(gameLogic.getLocationCount(), Location.Name.values().length)
              .stream()
              .filter(name -> location.getName().equalsIgnoreCase(name.toString()))
              .map(Enum::name)
              .findFirst();
      if (unusedLocation.isPresent()) {
        throw new RuntimeException(
            "Location '%s' cannot be used in this match.".formatted(unusedLocation.get()));
      }

      // Check for duplicate locations.
      if (processedLocationNames.contains(location.getName())) {
        throw new RuntimeException(
            "Location '%s' was already processed.".formatted(location.getName()));
      }

      // Fill card path lists.
      attackCardPaths.addAll(
          location.getAttackCardsList().stream().map(Message.Card::getPath).toList());
      defenseCardPaths.addAll(
          location.getDefenseCardsList().stream().map(Message.Card::getPath).toList());

      processedLocationNames.add(location.getName());

      validateCards(location, player);
    }
    processedLocationNames.sort(Comparator.naturalOrder());

    // Validate the client sent the right amount of att/def cards.
    if (gameLogic.getPlayers().size() == 2) {
      boolean hasBossPlayerRightCardAmounts =
          player.isExtraBoss()
              && attackCardPaths.size() == BOSS_MAX_ATTACK_CARDS
              && defenseCardPaths.size() == BOSS_MAX_DEFENSE_CARDS;
      boolean hasNonBossPlayerRightCardAmounts =
          !player.isExtraBoss()
              && attackCardPaths.size() == NON_BOSS_MAX_ATTACK_CARDS
              && defenseCardPaths.size() == NON_BOSS_MAX_DEFENSE_CARDS;

      if (!hasBossPlayerRightCardAmounts && !hasNonBossPlayerRightCardAmounts) {
        log.fatal(
            "Player {}{} sent {}/{} attack and {}/{} defense cards.",
            player.getColor().toString(),
            " (boss: %s)".formatted(player.isExtraBoss()),
            attackCardPaths.size(),
            player.isExtraBoss() ? BOSS_MAX_ATTACK_CARDS : NON_BOSS_MAX_ATTACK_CARDS,
            defenseCardPaths.size(),
            player.isExtraBoss() ? BOSS_MAX_DEFENSE_CARDS : NON_BOSS_MAX_DEFENSE_CARDS);
        throw new RuntimeException("Invalid amount of attack or defense cards.");
      }
    } else if (gameLogic.getPlayers().size() > 2
        && attackCardPaths.size() != 1
        && defenseCardPaths.size() != 1) {
      log.fatal(
          "Player {} sent {}/1 attack and {}/1 defense cards.",
          player.getColor().toString(),
          attackCardPaths.size(),
          defenseCardPaths.size());
      throw new RuntimeException("Invalid amount of attack or defense cards.");
    }

    // Validate that each of this round's locations was received exactly once.
    final List<String> locNames =
        Arrays.stream(Location.Name.values())
            .map(Enum::name)
            .toList()
            .subList(0, gameLogic.getLocationCount());
    if (!processedLocationNames.equals(locNames)) {
      log.fatal("Expected locations {}, but got {}.", locNames, processedLocationNames);
      throw new RuntimeException("Inconsistent locations.");
    }
  }

  private static void validateCards(Message.Location location, Player player) {
    List<Message.Card> allCards = new ArrayList<>(location.getAttackCardsList());
    allCards.addAll(location.getDefenseCardsList());
    validateCardPaths(allCards, location.getName());

    // Validate the client didn't send an att/def pair of the same card.
    List<String> attackCardPaths =
        location.getAttackCardsList().stream().map(Message.Card::getPath).toList();
    List<String> defenseCardPaths =
        location.getDefenseCardsList().stream().map(Message.Card::getPath).toList();
    if (!attackCardPaths.isEmpty() && !defenseCardPaths.isEmpty()) {
      log.fatal("Attack card paths: {}; defense card paths: {}", attackCardPaths, defenseCardPaths);
      throw new RuntimeException(
          "A single card cannot be attacking and defending at the same time.");
    }

    // Check for duplicate card paths.
    List<String> allPaths = new ArrayList<>(attackCardPaths);
    allPaths.addAll(defenseCardPaths);
    for (int i = 0; i < allPaths.size(); i++) {
      for (int j = 0; j < allPaths.size(); j++) {
        if (i == j) {
          continue;
        }

        if (allPaths.get(i).equals(allPaths.get(j))) {
          throw new RuntimeException(
              "Found duplicate card path of path '%s'.".formatted(allPaths.get(i)));
        }
      }
    }

    // Check that each card's color matches the player's color.
    if (!allCards.stream().allMatch(card -> card.getColor().equals(player.getColor()))) {
      log.fatal("Player color: {}; all playing cards: {}", player.getColor(), allCards);
      throw new RuntimeException("All playing cards must be of the player's color.");
    }
  }

  private static void validateCardPaths(List<Message.Card> cards, String locName) {
    Optional<Message.Card> invalidCard =
        cards.stream()
            .filter(
                card ->
                    !card.hasColor()
                        || !card.hasPath()
                        || !card.getPath().equals(getCardPath(card.getColor(), locName)))
            .findFirst();
    if (invalidCard.isPresent()) {
      Message.Card card = invalidCard.get();
      throw new RuntimeException(
          "Invalid card path: expected '%s', but got '%s'."
              .formatted(getCardPath(card.getColor(), locName), card.getPath()));
    }
  }

  private static String getCardPath(Message.PlayerColor color, String locName) {
    return "cards/playing/playing_%s_%s.png"
        .formatted(color.toString().toLowerCase(), locName.toLowerCase());
  }
}
