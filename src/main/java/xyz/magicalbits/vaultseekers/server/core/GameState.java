/**
 * Vault Seekers Server
 * Copyright (C) 2024  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.server.core;


import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import xyz.magicalbits.vaultseekers.common.Location;
import xyz.magicalbits.vaultseekers.common.card.VaultCard;
import xyz.magicalbits.vaultseekers.common.proto.Message;
import xyz.magicalbits.vaultseekers.server.utils.Utils;

@Getter
public class GameState {
  private final List<Message.Location.Builder> locations =
      Collections.synchronizedList(new ArrayList<>());
  private final Map<Location.Name, VaultCard> vaultCardSlots = new EnumMap<>(Location.Name.class);
  private final List<Message.PlayerColor> availablePlayerColors =
      Collections.synchronizedList(new ArrayList<>());
  private List<VaultCard> vaultOneCards = new ArrayList<>(20);
  private List<VaultCard> vaultTwoCards = new ArrayList<>(22);
  @Setter private volatile boolean isGameRunning = false;
  @Setter private volatile boolean isGameFinished = false;
  @Setter private int locationCount = 0;

  public void clear() {
    locations.clear();
    vaultCardSlots.clear();
    vaultOneCards.clear();
    vaultTwoCards.clear();
    availablePlayerColors.clear();
    isGameRunning = false;
    isGameFinished = false;
    locationCount = 0;
  }

  public void shuffleVaultCards() {
    this.vaultOneCards = Utils.shuffleVaultCards(vaultOneCards);
    this.vaultTwoCards = Utils.shuffleVaultCards(vaultTwoCards);
  }
}
