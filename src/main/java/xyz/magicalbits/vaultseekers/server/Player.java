/**
 * Vault Seekers Server
 * Copyright (C) 2023  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.server;


import io.netty.channel.Channel;
import java.util.EnumMap;
import java.util.Map;
import java.util.Random;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.proto.Gem;
import xyz.magicalbits.vaultseekers.common.proto.Message;

@Getter
@Log4j2
public class Player {
  private final Channel channel;
  private Message.PlayerColor color;
  private final Map<Gem.Color, Integer> gems = new EnumMap<>(Gem.Color.class);
  private final Map<Gem.Color, Integer> hiddenLootCards = new EnumMap<>(Gem.Color.class);
  @Setter private boolean extraBoss = false;
  @Setter private boolean turnEnded = false;
  private int points = 0;
  private final Map<Gem.Color, Integer> scores = new EnumMap<>(Gem.Color.class);
  @Setter private boolean winner = false;
  private boolean host = false;
  private final Random random = new Random();

  public Player(Channel channel) {
    this.channel = channel;
  }

  public void setColor(Message.PlayerColor color) {
    this.color = color;
    log.info("Assigned color {} to client {}", color, channel.remoteAddress());
  }

  public void addOneOfEachGems() {
    Map<Gem.Color, Integer> gemMap = new EnumMap<>(Gem.Color.class);
    Gem.Color[] values = Gem.Color.values();
    for (int i = 0; i < values.length - 1; i++) {
      Gem.Color gemColor = values[i];
      gemMap.put(gemColor, 1);
    }
    addGems(gemMap);
  }

  public void addGems(Gem.Color color, int amount) {
    Map<Gem.Color, Integer> gemMap = new EnumMap<>(Gem.Color.class);
    gemMap.put(color, amount);
    addGems(gemMap);
  }

  public void addGems(Map<Gem.Color, Integer> gems) {
    for (Map.Entry<Gem.Color, Integer> entry : gems.entrySet()) {
      final Gem.Color gemColor = entry.getKey();
      final Integer increment = entry.getValue();

      if (increment < 0) {
        throw new RuntimeException("Cannot add a negative amount of gems.");
      } else if (increment == 0) {
        log.warn("Skipping addition of zero gems.");
        continue;
      }

      final int currentAmount = this.gems.getOrDefault(gemColor, 0);
      final int newAmount = currentAmount + increment;
      this.gems.put(gemColor, newAmount);
      log.info(
          "Set {} gems of player {} to {} (was {}).", gemColor, color, newAmount, currentAmount);
    }
  }

  public void subtractGems(Gem.Color color, int amount) {
    if (amount < 0) {
      throw new IllegalArgumentException("Cannot subtract a negative amount of gems.");
    }

    int currentValue = gems.getOrDefault(color, 0);
    final int newValue = currentValue - amount;
    if (newValue >= 0) {
      gems.put(color, newValue);
    } else {
      throw new IllegalArgumentException("A player's gem amount cannot be negative.");
    }
  }

  public int getGemAmount(Gem.Color color) {
    return gems.getOrDefault(color, 0);
  }

  /** Returns the number of gems of the given color including gems from hidden loot cards. */
  public int getCompleteGemAmount(Gem.Color color) {
    return getGemAmount(color) + hiddenLootCards.getOrDefault(color, 0);
  }

  public void givePoints(int amount) {
    if (amount < 0) {
      throw new IllegalArgumentException("Cannot give a negative amount of points.");
    } else if (amount == 0) {
      log.warn("Skipping addition of zero points.");
      return;
    }

    log.info("Adding {} points to player {}.", amount, color);
    points += amount;
  }

  public void giveHiddenLootCards(int amount) {
    if (amount < 0) {
      throw new IllegalArgumentException("Hidden loot card amount cannot be negative.");
    } else if (amount == 0) {
      return;
    }

    for (int i = 0; i < amount; i++) {
      Gem.Color gemColor = Gem.Color.values()[random.nextInt(Gem.Color.values().length - 1)];
      hiddenLootCards.put(gemColor, hiddenLootCards.getOrDefault(gemColor, 0) + 1);
    }
  }

  public int getTotalHiddenLootCardCount() {
    return hiddenLootCards.values().stream().reduce(Integer::sum).orElse(0);
  }

  // public void revealHiddenLootCards() {
  //   List<Message.HiddenLootCard> cards = new ArrayList<>(4);
  //   for (int i = 0; i < Gem.Color.values().length - 1; i++) {
  //     Gem.Color gemColor = Gem.Color.values()[i];
  //     cards.add(
  //         Message.HiddenLootCard.newBuilder()
  //             .setColor(gemColor)
  //             .setAmount(hiddenLootCards.getOrDefault(gemColor, 0))
  //             .build());
  //   }
  //
  //   networkManager.sendBroadcastMessage(
  //       Message.newBuilder()
  //           .setType(Message.Type.UPDATE_PLAYER_STATS)
  //           .addPlayerStats(
  //               Message.PlayerStats.newBuilder()
  //                   .setColor(color)
  //                   .addAllHiddenLootCards(cards)
  //                   .build())
  //           .build());
  // }

  public void addScore(Gem.Color color, int amount) {
    if (amount < 0) {
      throw new IllegalArgumentException("Cannot add a negative score.");
    } else if (amount == 0) {
      return;
    }

    scores.put(color, scores.getOrDefault(color, 0) + amount);
  }

  public void setHost() {
    host = true;
  }
}
