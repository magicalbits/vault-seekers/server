/**
 * Vault Seekers Server
 * Copyright (C) 2024  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.server.utils;


import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import lombok.experimental.UtilityClass;
import xyz.magicalbits.vaultseekers.common.card.VaultCard;
import xyz.magicalbits.vaultseekers.common.proto.Gem;
import xyz.magicalbits.vaultseekers.common.proto.Message;
import xyz.magicalbits.vaultseekers.server.Player;

@UtilityClass
public final class Utils {
  private static final Random random = new Random();
  private static final String MSG_INVALID_PLAYER_COLOR = "Invalid player color.";

  public static boolean areMapsEqual(
      Map<Gem.Color, Integer> first, Map<Gem.Color, Integer> second) {
    if (first == null || second == null) {
      return false;
    }
    if (first.keySet().size() != second.keySet().size()) {
      return false;
    }
    if (!first.keySet().stream().allMatch(second::containsKey)) {
      return false;
    }
    return first.keySet().stream().allMatch(key -> second.get(key).equals(first.get(key)));
  }

  /** Calculates the sum of the player's gems which the local player selected for trading. */
  public static int getGemSum(Map<Gem.Color, Integer> gems) {
    return gems.values().stream().reduce(Integer::sum).orElse(0);
  }

  public static List<Message.PlayerStats.Score> convertScoreMapToList(
      Map<Gem.Color, Integer> scoreMap) {
    return Arrays.stream(Gem.Color.values())
        .filter(color -> !color.equals(Gem.Color.UNRECOGNIZED))
        .map(
            color -> {
              Message.PlayerStats.Score.Builder scoreBuilder =
                  Message.PlayerStats.Score.newBuilder();
              return scoreBuilder
                  .setGemColor(color)
                  .setAmount(scoreMap.getOrDefault(color, 0))
                  .build();
            })
        .toList();
  }

  public static List<VaultCard> shuffleVaultCards(List<VaultCard> cards) {
    int originalLength = cards.size();
    List<VaultCard> tmpCards = new ArrayList<>(originalLength);
    while (!cards.isEmpty()) {
      int randomIndex = random.nextInt(cards.size());
      tmpCards.add(cards.remove(randomIndex));
    }

    return tmpCards;
  }

  public static List<Message.HiddenLootCard> convertHiddenLootCardMapToList(
      Map<Gem.Color, Integer> cards) {
    List<Message.HiddenLootCard> cardList = new ArrayList<>(cards.size());
    for (Map.Entry<Gem.Color, Integer> entry : cards.entrySet()) {
      cardList.add(
          Message.HiddenLootCard.newBuilder()
              .setColor(entry.getKey())
              .setAmount(entry.getValue())
              .build());
    }
    return cardList;
  }

  public static List<Gem> convertGemMapToList(Map<Gem.Color, Integer> gems) {
    List<Gem> gemList = new ArrayList<>(gems.size());
    for (Map.Entry<Gem.Color, Integer> entry : gems.entrySet()) {
      gemList.add(Gem.newBuilder().setColor(entry.getKey()).setAmount(entry.getValue()).build());
    }
    return gemList;
  }

  public static List<Integer> getScoreRewards(Gem.Color color, int playerCount) {
    /*
     * Scorecards:
     *       gem:  blue |green|red  |yellow
     * 2 players:  40   |30   |30   |20
     * 3 players:  300  |310  |320  |330
     * 4 players:  3000 |3200 |3310 |3330
     * 5 players:  30000|33000|33300|33330
     *             |||||
     *             ||||L - zero score for the player with the least blue gems
     *             |||L - zero score for the fourth-richest player with blue gems
     *             ||L - zero score for the third-richest player with blue gems
     *             |L - zero score for the second-richest player with blue gems
     *             L - three score for the richest player with blue gems
     *
     * Each number in the scorecard table, such as "40" or "310" are just concatenated digits of
     * score amounts. The first digit amounts to score given to the player with the most gems
     * of given color, the second digit to the second-richest player, and so on.
     */
    return switch (playerCount) {
      case 2 -> switch (color) {
        case BLUE -> List.of(4, 0);
        case GREEN, RED -> List.of(3, 0);
        case YELLOW -> List.of(2, 0);
        default -> throw new InvalidParameterException(MSG_INVALID_PLAYER_COLOR);
      };
      case 3 -> switch (color) {
        case BLUE -> List.of(3, 0, 0);
        case GREEN -> List.of(3, 1, 0);
        case RED -> List.of(3, 2, 0);
        case YELLOW -> List.of(3, 3, 0);
        default -> throw new InvalidParameterException(MSG_INVALID_PLAYER_COLOR);
      };
      case 4 -> switch (color) {
        case BLUE -> List.of(3, 0, 0, 0);
        case GREEN -> List.of(3, 2, 0, 0);
        case RED -> List.of(3, 3, 1, 0);
        case YELLOW -> List.of(3, 3, 3, 0);
        default -> throw new InvalidParameterException(MSG_INVALID_PLAYER_COLOR);
      };
      case 5 -> switch (color) {
        case BLUE -> List.of(3, 0, 0, 0, 0);
        case GREEN -> List.of(3, 3, 0, 0, 0);
        case RED -> List.of(3, 3, 3, 0, 0);
        case YELLOW -> List.of(3, 3, 3, 3, 0);
        default -> throw new InvalidParameterException(MSG_INVALID_PLAYER_COLOR);
      };
      default -> throw new InvalidParameterException("Invalid player count.");
    };
  }

  public static List<Message.PlayerColor> getPlayerColors() {
    // Exclude Message.PlayerColor.UNRECOGNIZED (last enum element).
    return new ArrayList<>(
        List.of(
            Arrays.copyOf(Message.PlayerColor.values(), Message.PlayerColor.values().length - 1)));
  }

  private static List<Gem> serializeGems(Map<Gem.Color, Integer> gems) {
    List<Gem> serializedGems = new ArrayList<>(4);
    for (Map.Entry<Gem.Color, Integer> entry : gems.entrySet()) {
      final Gem.Color gemColor = entry.getKey();
      final Integer amount = entry.getValue();
      serializedGems.add(Gem.newBuilder().setColor(gemColor).setAmount(amount).build());
    }
    return serializedGems;
  }

  public static Message createMessageWithUpdatedGems(Player player) {
    return Message.newBuilder()
        .setType(Message.Type.UPDATE_PLAYER_STATS)
        .addPlayerStats(
            Message.PlayerStats.newBuilder()
                .setColor(player.getColor())
                .addAllGems(serializeGems(player.getGems()))
                .build())
        .build();
  }

  public static Message createMessageWithUpdatedPoints(Player player) {
    return Message.newBuilder()
        .setType(Message.Type.UPDATE_PLAYER_STATS)
        .addPlayerStats(
            Message.PlayerStats.newBuilder()
                .setColor(player.getColor())
                .setPoints(player.getPoints())
                .build())
        .build();
  }

  public static Message createMessageWithTotalHiddenLootCardCount(Player player) {
    return Message.newBuilder()
        .setType(Message.Type.UPDATE_PLAYER_STATS)
        .addPlayerStats(
            Message.PlayerStats.newBuilder()
                .setColor(player.getColor())
                .setTotalHiddenLootCardCount(player.getTotalHiddenLootCardCount())
                .build())
        .build();
  }
}
