/**
 * Vault Seekers Server
 * Copyright (C) 2023  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.server;


import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.security.InvalidParameterException;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.proto.Message;
import xyz.magicalbits.vaultseekers.server.core.GameLogic;
import xyz.magicalbits.vaultseekers.server.core.MessageHandler;

/**
 * Handler for a server-side channel. This handler maintains stateful information which is specific
 * to a certain channel using member variables. Therefore, an instance of this handler can cover
 * only one channel. You have to create a new handler instance whenever you create a new channel and
 * insert this handler to avoid a race condition.
 */
@Log4j2
public class GameServerHandler extends SimpleChannelInboundHandler<Message> {
  public static final int PLAYER_MIN_LIMIT = 2;
  public static final int PLAYER_LIMIT =
      Integer.parseInt(System.getProperty("vaultseekers.player.limit", "5"));

  final GameLogic gameLogic;
  private final MessageHandler messageHandler;

  public GameServerHandler(GameLogic gameLogic, MessageHandler messageHandler) {
    this.gameLogic = gameLogic;
    this.messageHandler = messageHandler;
  }

  @Override
  public void handlerAdded(ChannelHandlerContext ctx) {
    log.info("Client {} connected.", ctx.channel().remoteAddress());
    Player player = new Player(ctx.channel());
    gameLogic.addPlayer(player);
  }

  @Override
  public void handlerRemoved(ChannelHandlerContext ctx) {
    log.info("Client {} disconnected.", ctx.channel().remoteAddress());
    gameLogic.removePlayer(ctx);
  }

  @Override
  public void channelRead0(ChannelHandlerContext ctx, Message msg) {
    if (!msg.hasType()) {
      throw new InvalidParameterException("Message type field must be set.");
    }

    Player player = gameLogic.getPlayer(ctx);
    messageHandler.process(msg, player);
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
    cause.printStackTrace();
    ctx.close();
  }
}
