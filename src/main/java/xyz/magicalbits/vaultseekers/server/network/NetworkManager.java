/**
 * Vault Seekers Server
 * Copyright (C) 2024  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.magicalbits.vaultseekers.server.network;


import java.util.List;
import lombok.extern.log4j.Log4j2;
import xyz.magicalbits.vaultseekers.common.proto.Message;
import xyz.magicalbits.vaultseekers.server.Player;

@Log4j2
public class NetworkManager {

  public void disconnect(Player player, String errorMessage) {
    sendMessage(
        Message.newBuilder()
            .setType(Message.Type.ERROR_MESSAGE)
            .setErrorMessage(errorMessage)
            .build(),
        player);
    player.getChannel().disconnect();
    player.getChannel().close();

    String id =
        player.getColor() == null
            ? "Client %s".formatted(player.getChannel().remoteAddress())
            : "Player %s".formatted(player.getColor());
    log.error("{} disconnected: {}", id, errorMessage);
  }

  public void sendMessage(Message msg, Player player) {
    player.getChannel().writeAndFlush(msg);
  }

  public void sendMessage(Message msg, List<Player> players) {
    log.info("Sending message of type {} to all players.", msg.getType());
    for (Player p : players) {
      sendMessage(msg, p);
    }
  }
}
